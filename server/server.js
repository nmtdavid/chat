var db = require('./db');
var ws = require('ws');
var COMMANDS = require('./commands.js')
var expressWs
var cache = require('./usersInRoomsCache.js')

module.exports.server = function(app) {
    expressWs = require('express-ws')(app)

    app.ws('/ws', function(socket, req){
        socket.key = socket._ultron.id;

        // handle errors by closing socket connection
        socket._receiver.onerror = function(e) {
            socket._receiver.flush();
            socket._receiver.messageBuffer = [];
            socket._receiver.cleanup();
            COMMANDS.logout.call(socket, null)
            socket.close();
        }

        socket.on('message', function(data) {
            try {
                var args = JSON.parse(data)
                var cmd = args.cmd
                var command = COMMANDS[cmd]

                if (command && args) {
                    command.call(socket, args);
                }
            }
            catch(e){
                socket.terminate()
                console.warn(e.stack)
            }
        })

        socket.on('close', function(){
            COMMANDS.logout.call(socket, null)
        })
    })
}

module.exports.sendToAddress = function (data, userAddress, client){
    module.exports.send(data, client);
}

// method for broadcasting to everyone in room
module.exports.broadcast = function (data, room) {
    // get obj w users and sockets {userA: socketKeyA, userB: socketKeyB}
    let usersObj = cache.getAllUsersInRoom(room);
    // create array of socketKeys
    let socketList = Object.values(usersObj);
    // iterate over socket clients
    expressWs.getWss().clients.forEach(function(client) {
        // if client's key is in socketKey array
        if ( socketList.includes(client.key) ) {
            // call send on that client/socket, passing in data
            module.exports.send(data, client);
        }
    });
}

// method for sending message to specific client
module.exports.send = function(data, client) {
    try {
        if (client.readyState == ws.OPEN) {
            client.send(JSON.stringify(data))
        }
    } catch (e) {
    }
}
