import React from 'react';
import ChatEntry from './ChatEntry.jsx';

function ChatList (props) {
	return <div className="chat-list-messages">
		{props.messages.map((entry,i) => {
        var EntryComponent = ChatEntry[entry.type];
         return (
             <div style={{marginBottom: '10px'}} key={i}>
             <EntryComponent
                 entry={entry}
                 getNicknameOrAddress={props.getNicknameOrAddress}
                 network={props.network}
             />
             </div>
         )}
		)}
	</div>

};

export default ChatList;
